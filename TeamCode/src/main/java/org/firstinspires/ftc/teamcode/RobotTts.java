package org.firstinspires.ftc.teamcode;

import android.content.Context;
import android.media.MediaPlayer;
import android.speech.tts.TextToSpeech;

import com.qualcomm.robotcore.util.ElapsedTime;

import java.io.IOException;
import java.util.EventListener;
import java.util.Locale;
import java.util.Random;

import static org.firstinspires.ftc.teamcode.RobotTts.Language.CHINESE;
import static org.firstinspires.ftc.teamcode.RobotTts.Language.ENGLISH;
import static org.firstinspires.ftc.teamcode.RobotTts.Language.GERMAN;
import static org.firstinspires.ftc.teamcode.RobotTts.Language.JAPANESE;
import static org.firstinspires.ftc.teamcode.RobotTts.Language.KOREAN;
import static org.firstinspires.ftc.teamcode.RobotTts.Language.RUSSIAN;

public class RobotTts extends Module{

    private boolean enabled = true;
    TextToSpeech tts;

    private RobotLog debugLogger;
    private ElapsedTime time;

    Random generator = new Random();

    Language[] languages = new Language[] {
            JAPANESE,
            ENGLISH,
            KOREAN,
            GERMAN,
            CHINESE,
            RUSSIAN
    };

    RobotTts(Context context, ElapsedTime time, RobotLog debugLogger) {
        tts = new TextToSpeech(context, null);
        tts.setPitch(1.5f);
        tts.setSpeechRate(1.5f);
        this.time = time;
        this.debugLogger = debugLogger;
        if (this.debugLogger.loggingEnabled) {
            this.debugLogger.addDbgMessage(
                    RobotLog.DbgLevel.INFO,
                    "TTS",
                    "Initialized"
            );
        }
    }

    RobotTts() {
        this.enabled = false;
    }

    void speak(String text) {
        if (this.enabled) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            if (debugLogger.isLoggingEnabled()) {
                debugLogger.addDbgMessage(
                        RobotLog.DbgLevel.INFO,
                        "TTS",
                        text
                );
            }
        }
    }

    void setLanguage() {
        if (this.enabled) {
            tts.setLanguage(langToLocale(lang));

            if (debugLogger.isLoggingEnabled()) {
                debugLogger.addDbgMessage(
                        RobotLog.DbgLevel.INFO,
                        "TTS",
                        "Language set to " + lang
                );
            }
        }
    }

    void setLanguage(Language newLanguage) {
        if (this.enabled) {
            lang = newLanguage;
            tts.setLanguage(langToLocale(lang));
            if (debugLogger.isLoggingEnabled()) {
                debugLogger.addDbgMessage(
                        RobotLog.DbgLevel.INFO,
                        "TTS",
                        "Language set to " + lang
                );
            }
        }
    }

    void stopTalking() {
        if (this.enabled) {
            tts.stop();
        }
    }

    String[] randomLines(){
        switch (lang) {
            case JAPANESE:
                return new String[] {
                        "鳩が飛んだわ,　鞄を持って, 昨日のあなたに, レッツゴー, レッツゴー"
                };
            case KOREAN:
                return new String[] {
                        "감사합니다",
                        "대박. 우리는 해냈다.",
                        "오빠, 내 마음이 펄럭 거든."
                };
            case CHINESE:
                return new String[] {
                        "我想我们已经输了",
                        "妈妈你看接了吗",
                        "我们想修这个机器人",
                        "机器猫，准备摧毁羊",
                        "四是四。十是十。十四是四十。四十是四十。四十四是四十四",
                        "吃 葡 萄 不 吐 葡 萄 皮 ,不 吃 葡 萄 倒 吐 葡 萄 皮"
                };
            case ENGLISH:
                return new String[] {
                        "Speed and power",
                        "Green is my pepper",
                        "Install Gentoo",
                        "Be your best",
                        "I have low self esteem",
                        "Moving up in the world doesn't mean using the lift, mate",
                        "If Anyone Tells Me 'It's Better to Have Loved and Lost Than to Never Have" +
                                "Loved at All' I Will Stab Them in the Face",
                        "To many men life is a failure; a poison-worm gnaws at their heart. " +
                                "Then at least let their dying be a success.",
                        "Existence, what does it matter? I exist on the best terms I can. The past " +
                                "is now part of my future, the present is well out of hand",
                        "I'm ashamed of what I've been put through, I'm ashamed of the person I am",
                        "I have a big personality"
                };
            case GERMAN:
                return new String[] {
                        "Geschwindigkeit und Kraft",
                        "Sei dein Bestes",
                        "Grün ist mein Pfeffer",
                        "Wir fahr'n fahr'n fahr'n auf der autobahn, Vors uns liegt ein weites tal, " +
                                "Die sonne scheint mit glitzerstrahl, Die fahrbahn ist ein graues band",
                        "Empfangen und genähret, vom Weibe wunderbar, kömmt er und sieht und höret, " +
                                "und nimmt des Trugs nicht wahr; gelüstet und begehret"
                };
            case RUSSIAN:
                return new String[] {
                        "Мороз и солнце, день чудесный! Еще ты дремлещь, друг прелестный - ",
                        "Я сказал",
                        "Что за чушь",
                        "Элементарно, дорогой друг",
                        "Друг"
                };
            default:
                return null;
        }
    }

    String getRandomLine() {
        int i = generator.nextInt(randomLines().length);
        return randomLines()[i];
    }

    Language randomLang() {
        int i = generator.nextInt(languages.length);
        return languages[i];
    }

    private Locale langToLocale(Language language) {
        switch (language) {
            case JAPANESE:
                return Locale.JAPAN;
            case KOREAN:
                return Locale.KOREA;
            case CHINESE:
                return Locale.CHINA;
            case ENGLISH:
                return Locale.UK;
            case GERMAN:
                return Locale.GERMANY;
            case RUSSIAN:
                return new Locale("ru", "RU");
            default:
                return Locale.ITALY;
        }
    }

    String welcomeText(){
        switch (lang) {
            case JAPANESE:
                return "Kawaii neko robotto-chan is ready, senpai";
            case KOREAN:
                return "안녕하세요";
            case CHINESE:
                return "大家好。我们开始";
            case ENGLISH:
                return "Robot is ready to disappoint";
            case GERMAN:
                return "Willkommen in unserem Panzerkampfwagen";
            case RUSSIAN:
                return "Доброе день друзья";
            default:
                return null;
        }
    }

    enum Language {
        ENGLISH,
        JAPANESE,
        KOREAN,
        CHINESE,
        GERMAN,
        RUSSIAN
    }

    Language lang;
}
